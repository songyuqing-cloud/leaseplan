# LeasePlanAssignment

API Automation with Selenium Rest-Assured, Java, Maven, Serenity BDD Framework and gitlab-ci

## Requirements:
Below dependencies needs to be installed/configured
- Java 11 or higher (JAVA_HOME and PATH in environmental variables)
- Maven (M2, MAVEN_HOME and PATH in environmental variables)
- IDE
- It would be nice to have "Cucumber for Java" and "Gherkin" plugins installed in your IDE for better understanding and working of cucumber

## Downloading Project:
- Using git command and clone with HTTPS:
git clone https://gitlab.com/yagheshoza/leaseplan.git

*or*

- Download the project from https://gitlab.com/yagheshoza/leaseplan.git

## Working on feature file
- Existing feature file is currently located at "src/test/java/features" covering both for Positive and Negative Scenarios.
- If any new feature file needs to be added for a new service, then the file should be created in the above mentioned path.
- If any existing feature file needs to be updated then, select the feature file. While adding new scenarios, need to make sure there not more than 10 scenarios in a feature file as a best practice. In that case a new feature file should be created.
- In case you do not want to pass any data values in feature file, please create a feature file with "Scenario" instead of "Scenario Outline" and remove the "Examples" section.
- All the steps you write should be in Gherkin language.
- When you start typing your steps, you will get options to reuse the steps. You can choose to select the existing step or create a new step based on your scenario to be tested.
- There is a separate file "APIResources" located at "src/test/java/Resources" which is used to store endpoints. 
	StatusAPI("/status"),
	ListofBooksAPI("/books"),
	GetBookTypeAPI("/books/{}"),
	UpdateOrder("/orders/{}"),
	GetOrder("/orders/{}"),
	PostOrder("/orders/"),
	GetAccessToken("/api-clients/");

If the resource is already declared, you can use the same by giving its constant name. Or else you can add a new resource by declaring constant.



## Working on Step Defination
- All the step Definations are currently located at "src/test/java/stepDefinations". For each service, one java file has been created. There is also a BaseStepDefination file in which step Definations which are common to all services are declared.
- Once you are done writing new steps in a feature file, either it will highlight if step defination has not been created. Or else when you execute the scenario in console you will get the step for which defination is not created with proper step defination format.
- In-case you choose to create a new step definition file, make sure you create it in the same package as that of the existing step definition.
- Once you created your default step defintion, put the logic you want the step definition to do.
- The baseurl is stored in config.properties which is being commonly used everywhere.


## Execution:

TestRunner file has been created under "src/test/java/cucumber.Runner" folder which is responsible for executing the Scenarios in the features.

1.. Run via terminal
```sh
mvn verify
```
2.. Run via gitlab CI pipeline
```sh
All commands are configure in .gitlab-ci.yml file. So you can directly run the project via gitlab ci pipeline
```

## Important points to remember:
- ***Scenarios covered:*** Positive and Negative scenarios mentioned in the url are covered.
- ***TestData:*** TestData can be changed for the Scenarios having Examples.
- ***Reporting:*** Serenity BDD framework default reporting is used in this project.
- ***Test Report:*** Test Report is saved in location - "target/site/serenity/index.html". Test report will be visible locally only after executing the project from your terminal with command "mvn verify"
- ***.gitignore:*** target/ are covered in .gitignore file
- ***CI pipeline:*** The gitlab ci pipeline has been configured.
- ***Artifacts:*** Artifacts after executing the pipeline can be downloaded from gitlab.
- ***Service Interactions:*** End to End scenarios are not covered yet as the motive was to test individual end points.
- ***Coverage:*** All end points are covered but not all scenarios are covered.









