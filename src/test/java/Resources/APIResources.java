package Resources;

/**
  	This class helps to declare API resources in a constant 
  	which can be directly pass to the Scenarios in Feature file
 **/

public enum APIResources {

	StatusAPI("/status"),
	ListofBooksAPI("/books"),
	GetBookTypeAPI("/books/{}"),
	UpdateOrder("/orders/{}"),
	GetOrder("/orders/{}"),
	PostOrder("/orders/"),
	GetAccessToken("/api-clients/");
	
	
	
	private String resource;
	
	APIResources(String resource)
	{
		this.resource = resource;
	}
	
	public String getResource() {
		return resource;
	}
	
}
