package Resources;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;



public class Utilities {
	
	public ResponseSpecification resspec;
	public static RequestSpecification req;
	public static RequestSpecification res;
	public static Response response;
	public TestDataBuilder data = new TestDataBuilder();
	
	public String getGlobalValue(String key) throws IOException {
		
		Properties prop = new Properties();
		FileInputStream fs = new FileInputStream("config.properties");
		prop.load(fs);
		return prop.getProperty(key);
		
	}
	
	
	public String getJsonPath(Response response, String key) {
		
		String resp = response.asString();
	    JsonPath js = new JsonPath(resp);
	    return js.get(key).toString();	
		
	}


	public RequestSpecification requestSpecification() throws IOException {
		
		if (req == null)
		{

			PrintStream log = new PrintStream(new FileOutputStream(getGlobalValue("log_file")));
			
			req = new RequestSpecBuilder().setBaseUri(getGlobalValue("baseURL"))
					.addFilter(RequestLoggingFilter.logRequestTo(log))
					.addFilter(ResponseLoggingFilter.logResponseTo(log))
					.setAccept(ContentType.JSON).build();
			
		}
		
		return req;
	}
	
	
}
