package Resources;

import org.json.simple.JSONObject;
import java.util.Map;


/**
 * API calls where JSON body needs to be send in the requests are covered in this class.
 * All the below methods returns JSON Object for the particular rest calls
   which is then handled at step definition file for validation
 */


public class TestDataBuilder {
	
	JSONObject body = new JSONObject();
	
	
	/**
     * This is method to send JSON body for adding and updating pet
     * @param pets
     * @return
     */
	
	public JSONObject prepareOrder(Map<String, String> order) {
      
        body.clear();
        body.put("bookId", order.get("bookId"));
		body.put("customerName",order.get("customerName"));
          
        return body;
		
	}	
	public String getRegisterdUserData() {
		
		return "{\r\n  \"clientName\": \"suleman\",\r\n  "+
		"\"clientEmail\": \"suleman@example.com\"\r\n}";
		
	}

	public String getOrderData() {
		
		return "{\r\n  \"bookId\": \"1\",\r\n  "+
		"\"customerName\": \"suleman\"\r\n}";
		
	}

	public String bodyforUpdateOrder(String name) {
		
		return "{\r\n \"customerName\": \"+name+\"\r\n}";
		
	}

}
