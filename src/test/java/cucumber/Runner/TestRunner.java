package cucumber.Runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    features="src/test/java/features/", 
    plugin = {
        "pretty",
        "json:target/cucumber.json", 
        "html:target/default-html-reports"
    }, 
	glue= {"stepDefinations"},
	tags = "@booktest")

public class TestRunner {

}
