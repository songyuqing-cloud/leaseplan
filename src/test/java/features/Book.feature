#Author: yagoza

Feature: Validating the API related to books for both negative and positive scenarios


@booktest
Scenario Outline: Verify if user is able to add book to order
Given User gives data to add order details
		|bookId		|<bookId>		|
		|customerName			|<customerName>		|
When user call "PostOrder" with "POST" http request
Then the API call should get success with status code 201

Examples:
|bookId	|customerName		|
|1		|suleman			|



@booktest
Scenario Outline: Verify if user is able to view an order with valid id
Given User gives "id"
When user call http request "GetOrder" with "GET" for <id>
Then the API call should get success with status code 200
And "id" in the response body should be "<id>"
Examples:
|id	|
|A-prZosuBLfIhz5dd-Ib8			|



@booktest
Scenario Outline: Verify if user is not able to view an order with invalid id
Given User gives "id"
When user call http request "GetOrder" with "GET" for <id>
Then the API call should get success with status code 404


Examples:
|id	|
|1234444			|


@booktest
Scenario Outline: Verify if user is able to update and existing order
Given order is added to list and order id is available
When user call update request for order <customerName>
Then the API call should get success with status code 200

Examples:
|customerName	|
|John			|



@booktest
Scenario: Verify if user is able to delete an order
Given order is added to list and order id is available
When user call the deleted api request
Then the API call should get success with status code 204




@booktest
Scenario Outline: Verify if user is able to find book details based on id
Given User gives "ID"
When user call "GetBookTypeAPI" with "GET" http request for <ID>
Then the API call should get success with status code 200
And "id" in the response body should be "<ID>"

Examples:
|ID	|
|1			|
|2			|
|3			|





@booktest
Scenario: Verify API is reachable
Given user sends the data for status request
When user call "StatusAPI" with "GET" http request 
Then the API call should get success with status code 200


@booktest
Scenario Outline: Verify error when getting access token for registerd user
Given user sends the data for access token request
When user call "GetAccessToken" with "POST" http request 
Then the API call should get success with status code 409
And response should contain error "<error>"


Examples: 
|error	|
|API client already registered.		|





