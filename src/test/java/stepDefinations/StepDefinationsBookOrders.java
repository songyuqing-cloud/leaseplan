package stepDefinations;

import cucumber.api.java.en.*;
import enums.Context;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;
import java.io.IOException;

import java.util.Map;

import Resources.APIResources;
import Resources.TestContext;
import Resources.Utilities;


public class StepDefinationsBookOrders  extends Utilities{

    static String ID;
	TestContext testContext = new TestContext();

    @Given("user sends the data for status request")
	public void User_sends_the_data_for_status_request() throws IOException {
		res = given().spec(requestSpecification()).contentType(ContentType.JSON);
	}

	@Given("user sends the data for access token request")
	public void User_sends_the_data_for_access_token_request() throws IOException {
		res = given().spec(requestSpecification()).contentType(ContentType.JSON)
		          .body(data.getRegisterdUserData());
	}
    
	@Given("User gives data to add order details")
	public void user_gives_data_to_add_order_details(Map<String,String> order) throws IOException {
		res = given().spec(requestSpecification()).headers("Authorization","Bearer " + getGlobalValue("accessToken")).contentType(ContentType.JSON)
		.body(data.prepareOrder(order));
	}


	@Then("response should contain error {string}")
	public void Response_should_contain_error(String error) {
	   
		String error1 = response.jsonPath().get("error");
		System.out.println(error1);
		assertEquals(error, error1);
	}

	@Then("{string} in the response body should be {string}")
	public void in_the_response_body_should_be(String key, String value) {
		
		assertEquals(getJsonPath(response, key), value);	
		
	}



	@Given("order is added to list and order id is available")
    public void order_is_added_to_list_and_order_id_is_available() throws IOException {
		res = given().spec(requestSpecification()).headers("Authorization","Bearer " + getGlobalValue("accessToken")).contentType(ContentType.JSON)
		.body(data.getOrderData());
		
		response = res.when().post(APIResources.PostOrder.getResource());

		assertEquals(201, response.getStatusCode());
		String orderId = response.jsonPath().get("orderId");
		System.out.println(orderId);

        testContext.scenarioContext.setContext(Context.Order_ID, orderId);


    }

    @When("user call update request for order {}")
    public void user_call_update_request_with_for_order(String name) throws IOException {
		res = given().spec(requestSpecification()).headers("Authorization","Bearer " + getGlobalValue("accessToken")).contentType(ContentType.JSON)
		.body(data.bodyforUpdateOrder(name));

		response =res.when().get(APIResources.UpdateOrder.getResource().replace("{}",  (String)testContext.scenarioContext.getContext(Context.Order_ID)));
    }

    @When("user call the deleted api request")
    public void user_call_delete_request_with_for_order() throws IOException {
		res = given().spec(requestSpecification()).headers("Authorization","Bearer " + getGlobalValue("accessToken")).contentType(ContentType.JSON);

		response =res.when().delete(APIResources.UpdateOrder.getResource().replace("{}",  (String)testContext.scenarioContext.getContext(Context.Order_ID)));
    }

	@Given("No Additional Details given")
	public void No_Additional_Details_given() throws IOException {
		
		res = given().spec(requestSpecification());
		
	}

	
    
}
